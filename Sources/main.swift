import Vapor

let drop = Droplet()

drop.get("/") { _ in
    return "Hello world"
}

// In a real server, we'd use a database to store info instead
var names = [String]()

drop.post("/names") { request in
    if let name = request.data["name"]?.string {
        names.append(name)
        // status code 201 created
        return "Added"
    } else {
        // Return the appropriate status code here
        // probably 400
        return "Wha?"
    }
}

drop.get("/names") { _ in
    // Turn this into JSON and set the content-type header
    return "\(names)"
}

drop.run()
